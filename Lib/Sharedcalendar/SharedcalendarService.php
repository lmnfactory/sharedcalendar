<?php

namespace Lmn\Sharedcalendar\Lib\Sharedcalendar;

use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\CurrentUser;
use Lmn\Sharedcalendar\Database\Model\CalendareventUser;
use Lmn\Sharedcalendar\Repository\CalendareventUserRepository;
use Lmn\Calendar\Repository\CalendareventRepository;
use Lmn\Core\Lib\Filter\FilterService;
use Lmn\Subject\Lib\Subject\SubjectUserService;
use Lmn\Notification\Lib\NotificationDb\NotificationService;
use Lmn\Notification\Repository\NotificationRepository;

use Lmn\Core\Lib\Database\Select\ConditionCollection;
use Lmn\Core\Lib\Database\Select\ConditionCollectionBuilder;

class SharedcalendarService {

    private $calendarUserRepo;
    private $calendareventRepo;
    private $subjectuserService;
    private $notificationService;
    private $notificationRepo;
    private $currentUser;

    public function __construct(CalendareventUserRepository $calendarUserRepo, CalendareventRepository $calendareventRepo, SubjectUserService $subjecuserService, NotificationService $notificationService, CurrentUser $currentUser, NotificationRepository $notificationRepo) {
        $this->calendarUserRepo = $calendarUserRepo;
        $this->calendareventRepo = $calendareventRepo;
        $this->subjectuserService = $subjecuserService;
        $this->notificationService = $notificationService;
        $this->notificationRepo = $notificationRepo;
        $this->currentUser = $currentUser;
    }

    private function getSubjectUsers($subjectId) {
        $subjectUsers = $this->subjectuserService->forSubject($subjectId);
        $users = [];
        foreach ($subjectUsers as $subjectUser) {
            if ($subjectUser->user_id == $this->currentUser->getId()) {
                continue;
            }
            $users[] = $subjectUser->user_id;
        }

        return $users;
    }

    public function userCalendar($userId, $since, $until) {
        $subjects = $this->subjectuserService->forUser($userId);

        $subjectIds = [];
        foreach ($subjects as $s) {
            $subjectIds[] = $s->subject_id;
        }

        $users = $this->calendarUserRepo->clear()
            ->criteria('calendareventuser.by.user', ['userId' => $userId, 'subjectIds' => $subjectIds])
            ->criteria('calendareventuser.with.calendarevent')
            ->criteria('calendarevent.by.range', ['since' => $since, 'until' => $until])
            ->criteria('calendarevent.by.range', ['since' => $since, 'until' => $until])
            ->all();

        $eventIds = [];
        foreach ($users as $userEvent) {
            $eventIds[] = $userEvent->calendarevent_id;
        }

        return $this->calendareventRepo->clear()
            ->criteria('core.ids', ['ids' => $eventIds])
            ->criteria('calendarevent.order.default')
            ->all();
    }

    public function subscribe($calendareventId) {
        /** @var AuthService $authService */
        $authService = \App::make(AuthService::class);
        $user = $authService->getCurrentUser();

        $event = CalendareventUser::where([
                'user_id' => $user->id,
                'calendarevent_id' => $calendareventId
            ])->first();

        if ($event != null) {
            throw new ItemInDatabaseException("Calendar event already added.");
        }

        $event = new CalendareventUser();
        $event->user_id = $user->id;
        $event->$calendarevent_id =$calendareventId;
        $event->save();

        return $event;
    }

    public function unsubscribe($calendarId) {
        CalendareventUser::where([
            'user_id' => $user->id,
            'calendarevent_id' => $calendareventId
        ])->delete();
    }

    public function share($calendarevent) 
    {
        if (!isset($calendarevent->calendareventsubject)) {
            return;
        }
        $users = $this->getSubjectUsers($calendarevent->calendareventsubject->subject_id);

        $subject = $calendarevent->calendareventsubject->subject;
        foreach ($users as $uid) {
            $notification = $this->notificationRepo->clear()
            ->create([
                'source' => 'calendarevent.create',
                'source_id' => $calendarevent->id,
                'user_id' => $uid,
                'title' => $this->currentUser->getFullname() . ' vytvoril udalosť',
                'text' => $calendarevent->name,
                'options' => ['subjectPid' => $subject->subjectprototype->public_id]
            ]);
            $this->notificationService->pushNotification($notification)->send();
        }
    }
}
