<?php

namespace Lmn\Sharedcalendar\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Lmn\Core\Lib\Response\ResponseService;
use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Database\Select\ConditionCollectionBuilder;
use Lmn\Core\Lib\Filter\FilterService;

use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Account\Lib\Auth\CurrentUser;

use Lmn\Notification\Lib\Notification\NotificationService;

use Lmn\Calendar\Lib\Calendar\CalendareventService;
use Lmn\Calendar\Repository\CalendareventRepository;
use Lmn\Sharedcalendar\Lib\Sharedcalendar\SharedcalendarService;
use Lmn\Sharedcalendar\Repository\CalendareventUserRepository;

class SharedcalendarController extends Controller {

    public function __construct() {

    }

    public function create(Request $request, ResponseService $responseService, ValidationService $validationService, CalendareventRepository $calendareventRepo, SharedcalendarService $sharedcalendareventService) {
        $data = $request->json()->all();

        if (!$validationService->validate($data, 'form.calendar.create')) {
            return $responseService->use('validation.data');
        }

        $calendarevent = $calendareventRepo->clear()
            ->create($data);

        if (isset($data['share']) && $data['share']) {
            $sharedcalendareventService->share($calendarevent);
        }

        return $responseService->response($calendarevent->toArray());
    }

    public function update(Request $request, ResponseService $responseService, ValidationService $validationService, CalendareventRepository $calendareventRepo, SharedcalendarService $sharedcalendareventService) {
        $data = $request->json()->all();

        if (!$validationService->validate($data, 'form.calendar.update')) {
            return $responseService->use('validation.data');
        }

        $calendarevent = $calendareventRepo->clear()
            ->criteria('calendarevent.by.pid', ['publicId' => $data['public_id']])
            ->update($data);

        if (isset($data['share']) && $data['share']) {
            $sharedcalendareventService->share($calendarevent);
        }

        return $responseService->response($calendarevent->toArray());
    }

    public function userCalendar(Request $request, ResponseService $responseService, CurrentUser $currentUser, ValidationService $validationService, SharedcalendarService $sharedcalendareventService, CalendareventService $calendareventService, CalendareventUserRepository $calendarUserRepo) {
        $data = $request->json()->all();

        $events = $sharedcalendareventService->userCalendar($currentUser->getId(), $data['since'], $data['until']);

        $eventsForDay = $calendareventService->getEventsInRange($events, new \DateTime($data['since']), new \DateTime($data['until']));

        $message = $responseService->createMessage($eventsForDay);
        $message->setOption($events);

        return $responseService->send($message);
    }

    public function join(Request $request, ResponseService $responseService, ValidationService $validationService, CalendareventUserRepository $calendareventUserRepo, CurrentUser $currentUser) {
        $data = $request->json()->all();
        $data['user_id'] = $currentUser->getId();

        $validationService->systemValidate($data, 'calendarevent.join');

        $calendarevent = $calendareventUserRepo->clear()
            ->create($data);

        return $responseService->response($calendarevent);
    }

    public function leave(Request $request, ResponseService $responseService, ValidationService $validationService, CalendareventUserRepository $calendareventUserRepo, CurrentUser $currentUser) {
        $data = $request->json()->all();

        $validationService->systemValidate($data, 'calendarevent.leave');

        $calendarevent = $calendareventUserRepo->clear()
            ->criteria('calendareventuser.unique', ['userId' => $currentUser->getId(), 'calendareventId' => $data['calendarevent_id']])
            ->delete();

        return $responseService->response($calendarevent);
    }

    public function subjectCalendar(Request $request, ResponseService $responseService, CurrentUser $currentUser, ValidationService $validationService, CalendareventRepository $calendarRepo) {
        $data = $request->json()->all();

        $validationService->systemValidate($data, 'subject.public_id');

        $events = $calendarRepo->clear()
            ->criteria('calendarevent.with.settings')
            ->criteria('calendareventsettings.publicOrOwn', ['userId' => $currentUser->getId()])
            ->criteria('calendar.with.calendareventsubject')
            ->criteria('calendareventsubject.by.subject', ['subjectPid' => $data['subject_pid']])
            ->criteria('calendareventsubject.with.user', ['userId' => $currentUser->getId()])
            ->all();

        $message = $responseService->createMessage($events);
        $message->setOption([
            'totalItems' => count($events)
        ]);

        return $responseService->send($message);
    }
}
