<?php

namespace Lmn\Sharedcalendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventLeaveValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'calendarevent_id' => 'required|exists:calendarevent,id'
        ];
    }
}
