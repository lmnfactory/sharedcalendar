<?php

namespace Lmn\Sharedcalendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventJoinValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'calendarevent_id' => 'required|exists:calendarevent,id'
        ];
    }
}
