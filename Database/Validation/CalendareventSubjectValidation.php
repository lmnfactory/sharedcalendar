<?php

namespace Lmn\Sharedcalendar\Database\Validation;
use Lmn\Core\Lib\Model\LaravelValidation;

class CalendareventSubjectValidation extends LaravelValidation {

    public function getRules($data) {
        return [
            'subject_id' => 'required|exists:subject,id'
        ];
    }
}
