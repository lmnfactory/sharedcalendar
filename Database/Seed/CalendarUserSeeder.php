<?php

namespace Lmn\Sharedcalendar\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class CalendarUserSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('calendarevent_user')->insert([
            [
                'id' => 1,
                'calendarevent_id' => 1,
                'user_id' => 1,
            ],
            [
                'id' => 2,
                'calendarevent_id' => 2,
                'user_id' => 1,
            ],
            [
                'id' => 3,
                'calendarevent_id' => 3,
                'user_id' => 1,
            ],
        ]);
    }
}
