<?php

namespace Lmn\Sharedcalendar\Database\Seed;

use App;
use Illuminate\Database\Seeder;

class CalendarSubjectSeeder extends Seeder {

    public function run() {
        $env = App::environment();
        if ($env == "production") {
            return;
        }
        
        \DB::table('calendarevent_subject')->insert([
            [
                'id' => 1,
                'calendarevent_id' => 1,
                'subject_id' => 1,
            ],
            [
                'id' => 2,
                'calendarevent_id' => 2,
                'subject_id' => 1,
            ],
            [
                'id' => 3,
                'calendarevent_id' => 3,
                'subject_id' => 2,
            ],
        ]);
    }
}
