<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarevent_subject', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendarevent_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->timestamps();

            $table->index('calendarevent_id');
            $table->index('subject_id');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarevent_subject');
    }
}
