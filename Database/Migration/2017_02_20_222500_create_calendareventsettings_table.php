<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendareventsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendarevent_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('private')->default(0);
            $table->timestamps();

            $table->index('calendarevent_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendareventsettings');
    }
}
