<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendareventUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarevent_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendarevent_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->index('calendarevent_id');
            $table->index('user_id');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarevent_user');
    }
}
