<?php

namespace Lmn\Sharedcalendar\Database\Model;

use Illuminate\Database\Eloquent\Model;
use Lmn\Subject\Database\Model\Subject;

class CalendareventSubject extends Model {

    protected $table = 'calendarevent_subject';

    protected $fillable = ['calendarevent_id', 'subject_id'];

    protected function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
