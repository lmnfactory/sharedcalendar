<?php

namespace Lmn\Sharedcalendar\Database\Model;

use Illuminate\Database\Eloquent\Model;

class CalendareventUser extends Model {

    protected $table = 'calendarevent_user';

    protected $fillable = ['calendarevent_id', 'user_id'];
}
