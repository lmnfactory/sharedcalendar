<?php

namespace Lmn\Sharedcalendar\Database\Model;

use Illuminate\Database\Eloquent\Model;

class Calendareventsettings extends Model {

    protected $table = 'calendareventsettings';

    protected $fillable = ['calendarevent_id', 'user_id', 'private'];
}
