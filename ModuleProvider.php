<?php

namespace Lmn\Sharedcalendar;

use Lmn\Core\Provider\LmnServiceProvider;
use Lmn\Core\Lib\Module\ModuleServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;

use Lmn\Core\Lib\Database\Seeder\SeederService;
use Lmn\Core\Lib\Cache\CacheService;

use Lmn\Core\Lib\Model\ValidationService;
use Lmn\Core\Lib\Repository\Config\RepositoryConfig;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;

use Lmn\Calendar\Repository\CalendareventRepository;

use Lmn\Sharedcalendar\Database\Seed\CalendarSubjectSeeder;
use Lmn\Sharedcalendar\Database\Seed\CalendarUserSeeder;

use Lmn\Sharedcalendar\Database\Validation\CalendareventsettingsValidation;
use Lmn\Sharedcalendar\Database\Validation\CalendareventSubjectValidation;
use Lmn\Sharedcalendar\Database\Validation\CalendareventJoinValidation;
use Lmn\Sharedcalendar\Database\Validation\CalendareventLeaveValidation;

use Lmn\Sharedcalendar\Lib\Sharedcalendar\SharedcalendareventService;

use Lmn\Sharedcalendar\Repository\CalendareventsettingsRepository;
use Lmn\Sharedcalendar\Repository\Listener\CalendareventUserListener;
use Lmn\Sharedcalendar\Repository\Listener\CalendareventSubjectListener;
use Lmn\Sharedcalendar\Repository\Listener\CalendareventsettingsListener;
use Lmn\Sharedcalendar\Repository\CalendareventSubjectRepository;
use Lmn\Sharedcalendar\Repository\CalendareventUserRepository;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser\CalendareventuserByUserCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser\CalendareventuserWithCalendarCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser\CalendareventuserUniqueCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventsubject\CalendareventSubjectBySubjectPidCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventsubject\CalendareventWithCalendareventSubjectCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventsubject\CalendareventSubjectWithUserCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventsettings\CalendareventWithSettingsCriteria;
use Lmn\Sharedcalendar\Repository\Criteria\Calendareventsettings\CalendareventsettingsPublicOrOwnCriteria;

use Lmn\Account\Middleware\SigninRequiredMiddleware;

class ModuleProvider implements ModuleServiceProvider{

    public function register(LmnServiceProvider $provider) {
        /** @var Application */
        $app = $provider->getApp();

        $app->singleton(SharedcalendareventService::class, function($app) {
            return new SharedcalendareventService();
        });

        $app->singleton(CalendareventsettingsRepository::class, CalendareventsettingsRepository::class);
        $app->singleton(CalendareventUserRepository::class, CalendareventUserRepository::class);
        $app->singleton(CalendareventSubjectRepository::class, CalendareventSubjectRepository::class);
        $app->singleton(CalendareventsettingsRepositoryExtension::class, CalendareventsettingsRepositoryExtension::class);
        $app->singleton(CalendareventUserRepositoryExtension::class, CalendareventUserRepositoryExtension::class);
        $app->singleton(CalendareventSubjectRepositoryExtension::class, CalendareventSubjectRepositoryExtension::class);
    }

    public function boot(LmnServiceProvider $provider) {
        /** @var Application */
        $app = $provider->getApp();

        /** @var SeederService */
        $seederService = $app->make(SeederService::class);
        $seederService->addSeeder(CalendarSubjectSeeder::class);
        $seederService->addSeeder(CalendarUserSeeder::class);

        /** @var ValidationService $validationService */
        $validationService = \App::make(ValidationService::class);
        $validationService->add('calendareventsettings', CalendareventsettingsValidation::class);
        $validationService->add('calendarevent_subject', CalendareventSubjectValidation::class);
        $validationService->add('calendarevent.join', CalendareventJoinValidation::class);
        $validationService->add('calendarevent.leave', CalendareventLeaveValidation::class);

        /** @var CriteriaService */
        $criteriaService = $app->make(CriteriaService::class);
        $criteriaService->add('calendareventuser.with.calendarevent', CalendareventuserWithCalendarCriteria::class);
        $criteriaService->add('calendareventuser.by.user', CalendareventuserByUserCriteria::class);
        $criteriaService->add('calendareventuser.unique', CalendareventuserUniqueCriteria::class);
        $criteriaService->add('calendareventsubject.by.subject', CalendareventSubjectBySubjectPidCriteria::class);
        $criteriaService->add('calendar.with.calendareventsubject', CalendareventWithCalendareventSubjectCriteria::class);
        $criteriaService->add('calendareventsubject.with.user', CalendareventSubjectWithUserCriteria::class);
        $criteriaService->add('calendarevent.with.settings', CalendareventWithSettingsCriteria::class);
        $criteriaService->add('calendareventsettings.publicOrOwn', CalendareventsettingsPublicOrOwnCriteria::class);

        /** @var CalendareventRepository $calendareventRepository */
        $calendareventRepository = $app->make(CalendareventRepository::class);
        $calendareventRepository->on('*', new CalendareventsettingsListener($app->make(CalendareventsettingsRepository::class)));
        $calendareventRepository->on('*', new CalendareventUserListener($app->make(CalendareventUserRepository::class)));
        $calendareventRepository->on('*', new CalendareventSubjectListener($app->make(CalendareventSubjectRepository::class)));
    }

    public function event(LmnServiceProvider $provider) {

    }

    public function route(LmnServiceProvider $provider) {
        Route::group(['namespace' => 'Lmn\\Sharedcalendar\\Controller'], function() {
            Route::any('api/sharedcalendar/user_calendar', 'SharedcalendarController@userCalendar')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/sharedcalendar/subject_list', 'SharedcalendarController@subjectCalendar')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/calendar/create', 'SharedcalendarController@create')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/calendar/update', 'SharedcalendarController@update')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/sharedcalendar/join', 'SharedcalendarController@join')->middleware(SigninRequiredMiddleware::class);
            Route::any('api/sharedcalendar/leave', 'SharedcalendarController@leave')->middleware(SigninRequiredMiddleware::class);
        });
    }
}
