<?php

namespace Lmn\Sharedcalendar\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Sharedcalendar\Database\Model\CalendareventUser;

class CalendareventUserRepository extends AbstractEloquentRepository {

    private $authService;

    public function __construct(CriteriaService $criteriaService, AuthService $authService) {
        parent::__construct($criteriaService);
        $this->authService = $authService;
    }

    public function getModel() {
        return CalendareventUser::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $calendarUser = new $model();

        $user = $this->authService->getCurrentUser();

        $calendarUser->fill($data);
        $calendarUser->user_id = $user->id;

        $calendarUser->save();
        return $calendarUser;
    }
}
