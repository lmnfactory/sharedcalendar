<?php

namespace Lmn\Sharedcalendar\Repository;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Sharedcalendar\Database\Model\CalendareventSubject;

class CalendareventSubjectRepository extends AbstractEloquentRepository {

    private $authService;

    public function __construct(CriteriaService $criteriaService) {
        parent::__construct($criteriaService);
    }

    public function getModel() {
        return CalendareventSubject::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $calendarSubject = new $model();

        $calendarSubject->fill($data);

        $calendarSubject->save();
        return $calendarSubject;
    }
}
