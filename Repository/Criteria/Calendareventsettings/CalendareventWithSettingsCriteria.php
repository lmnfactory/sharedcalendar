<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventsettings;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventWithSettingsCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) {
        
    }

    public function apply(Builder $builder) {
        $builder->leftJoin('calendareventsettings', 'calendareventsettings.calendarevent_id', '=', 'calendarevent.id');
    }
}
