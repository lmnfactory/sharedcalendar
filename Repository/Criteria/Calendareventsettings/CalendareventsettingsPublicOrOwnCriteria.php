<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventsettings;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventsettingsPublicOrOwnCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($args) {
        $this->userId = $args['userId'];
    }

    public function apply(Builder $builder) {
        $builder->where(function($query) {
            $query->whereNull('calendareventsettings.private')
                ->orWhere('calendareventsettings.private', '=', false)
                ->orWhere('calendareventsettings.user_id', '=', $this->userId);
        });
    }
}
