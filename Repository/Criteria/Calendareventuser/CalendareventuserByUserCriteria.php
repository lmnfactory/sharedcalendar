<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventuserByUserCriteria implements Criteria {

    private $userId;
    private $subjectIds;

    public function __construct() {

    }

    public function set($args) {
        $this->userId = $args['userId'];
        $this->subjectIds = $args['subjectIds'];
        $this->subjectIds[] = 0;
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->leftJoin('calendarevent_subject', 'calendarevent_subject.calendarevent_id', '=', 'calendarevent_user.calendarevent_id')
            ->where('user_id', '=', $this->userId)
            ->whereIn('subject_id', $this->subjectIds);
    }
}
