<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventuserUniqueCriteria implements Criteria {

    private $userId;
    private $calendareventId;

    public function __construct() {

    }

    public function set($args) {
        $this->userId = $args['userId'];
        $this->calendareventId = $args['calendareventId'];
    }

    public function apply(Builder $builder) {
        $builder->where('user_id', '=', $this->userId)
            ->where('calendarevent_id', '=', $this->calendareventId);
    }
}
