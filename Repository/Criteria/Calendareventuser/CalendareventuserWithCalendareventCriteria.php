<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventuser;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventuserWithCalendarCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $builder) {
        $builder->join('calendarevent', 'calendarevent_user.calendarevent_id', '=', 'calendarevent.id');
    }
}
