<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventsubject;

use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CalendareventWithCalendareventSubjectCriteria implements Criteria {

    public function __construct() {

    }

    public function set($args) {

    }

    public function apply(Builder $builder) {
        $builder->join('calendarevent_subject', 'calendarevent_subject.calendarevent_id', '=', 'calendarevent.id');
    }
}
