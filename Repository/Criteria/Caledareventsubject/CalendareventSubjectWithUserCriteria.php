<?php

namespace Lmn\Sharedcalendar\Repository\Criteria\Calendareventsubject;

use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\Criteria;
use Lmn\Core\Lib\Repository\Criteria\EloquentCriteriaService;
use Illuminate\Database\Eloquent\Builder;

class CalendareventSubjectWithUserCriteria implements Criteria {

    private $userId;

    public function __construct() {

    }

    public function set($data) {
        $this->userId = $data['userId'];
    }

    public function apply(Builder $builder) {
        $self = $this;
        $builder->addSelect(['calendarevent_user.user_id'])
            ->leftJoin('calendarevent_user', function($join) use ($self) {
                $join->on('calendarevent_user.calendarevent_id', '=', 'calendarevent_subject.calendarevent_id')
                    ->on('calendarevent_user.user_id', '=', \DB::raw($self->userId));
            });
    }
}
