<?php

namespace Lmn\Sharedcalendar\Repository\Listener;

use Lmn\Core\Lib\Repository\EloquentRepository;
use Lmn\Core\Lib\Repository\AbstractListenerEloquentRepository;

class CalendareventsettingsListener extends AbstractListenerEloquentRepository {

    private $repo;

    public function __construct(EloquentRepository $repo) {
        $this->repo = $repo;
    }

    private function set($parentModel, $value) {
        $parentModel->calendareventsettings = $value;
    }

    public function onGet($model) {
        if (!$model) {
            return;
        }
        $value = $this->repo->clear()
            ->criteria('calendarevent.ext.id', ['calendareventId' => $model->id])
            ->get();
        $this->set($model, $value);
    }

    public function onList($models) {
        $ids = [];
        foreach ($models as $m) {
            $ids[] = $m->id;
        }

        $list = $this->repo->clear()
            ->criteria('calendarevent.ext.ids', ['calendareventIds' => $ids])
            ->all();

        $listSorted = [];
        foreach ($list as $l) {
            $listSorted[$l->calendarevent_id] = $l;
        }
        foreach ($models as $model) {
            if (!isset($listSorted[$model->id])) {
                $this->set($model, null);
                continue;
            }
            $this->set($model, $listSorted[$model->id]);
        }
    }

    public function onCreate($model, $data) {
        if (!$model) {
            return null;
        }
        if (!isset($data['calendareventsettings'])) {
            $data['calendareventsettings'] = [];
        }

        $createData = $data['calendareventsettings'];
        $createData['calendarevent_id'] = $model->id;

        $this->set($model, $this->repo->create($createData));
    }
}
