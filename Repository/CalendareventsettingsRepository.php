<?php

namespace Lmn\Sharedcalendar\Repository;
use Lmn\Core\Lib\Repository\AbstractEloquentRepository;
use Lmn\Core\Lib\Repository\Criteria\CriteriaService;
use Lmn\Account\Lib\Auth\AuthService;
use Lmn\Sharedcalendar\Database\Model\Calendareventsettings;

class CalendareventsettingsRepository extends AbstractEloquentRepository {

    private $authService;

    public function __construct(CriteriaService $criteriaService, AuthService $authService) {
        parent::__construct($criteriaService);
        $this->authService = $authService;
    }

    public function getModel() {
        return Calendareventsettings::class;
    }

    public function create($data) {
        $model = $this->getModel();
        $settings = new $model();

        $user = $this->authService->getCurrentUser();

        $settings->fill($data);
        $settings->user_id = $user->id;

        $settings->save();
        return $settings;
    }
}
